import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationServiceService } from '../authentication-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  constructor(private router: Router,private fb: FormBuilder,
    private loginservice : AuthenticationServiceService ) { }
 form: FormGroup;
  invalidLogin = false
  ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  checkLogin() {
    if (this.loginservice.authenticate((this.form.value.username).toString(),(this.form.value.password).toString())
    ) {
      this.router.navigate([''])
      this.invalidLogin = false
    } else
      this.invalidLogin = true
      this.router.navigate(['/subject'])
  }
 
}