import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationServiceService } from '../authentication-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  username: string;
  password: string;
  constructor(private router: Router,private fb: FormBuilder,
    private loginservice : AuthenticationServiceService) { }
 form: FormGroup;
  ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
   register(){
     console.log(this.form.value.username)
     this.loginservice.register((this.form.value.username).toString(),(this.form.value.password).toString());
     this.router.navigate(['/login'])
   }

}
